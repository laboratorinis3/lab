<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="head.jsp"%>
        <title>Profile</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <form role="form" action="<c:url value="/logout" />" method="GET">
                        <button type="submit" class="btn btn-default">Log out</button>
                    </form>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <form role="form" action="<c:url value="/profile/save" />" method="POST">
                        <div class="row">
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-default" disabled="true">Edit</button>
                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-default">Save</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="firstname">First Name</label>
                                    <input type="text" class="form-control" 
                                           name="firstname" id="firstname" 
                                           placeholder="First Name"
                                           value="${user.firstname}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="lastname">Last Name</label>
                                    <input type="text" class="form-control" 
                                           name="lastname" id="lastname" 
                                           placeholder="Last Name"
                                           value="${user.lastname}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" 
                                           name="email" id="email" 
                                           placeholder="Email address"
                                           value="${user.email}">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
