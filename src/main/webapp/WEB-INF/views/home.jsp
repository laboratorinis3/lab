<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
    <head>
        <%@ include file="head.jsp"%>
        <title>Home</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <form role="form" action="<c:url value="/logout" />" method="GET">
                        <button type="submit" class="btn btn-default">Log out</button>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label for="role">Role</label>
                    <select class="form-control" name="role" title="Role" id="role">
                        <option></option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label for="name">Name</label>
                    <select class="form-control" name="name" title="Name" id="name">
                        <option></option>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label for="profile">Profile</label>
                    <form role="form" id="profile" action="<c:url value="/profile" />" method="GET">
                        <button type="submit" class="btn btn-default">View</button>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <form role="form" action="<c:url value="/" />" method="GET">
                        <label for="course">Course</label>
                        <select class="form-control" name="course" title="Course" id="course">
                            <option></option>
                        </select>
                        <button type="submit" class="btn btn-default">Choose</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
