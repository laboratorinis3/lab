package com.uni.lab.services;

import com.uni.lab.domain.Users;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

public interface UserService {
    
    public ModelAndView checkLoginStatusAndReturnModel(String currentModelName,
            HttpServletRequest request);
    
    public Boolean checkLoginStatus(HttpServletRequest request);
    
    public void saveUser(Users user);
}
