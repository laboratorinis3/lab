package com.uni.lab.services;

import com.uni.lab.dao.GenericDao;
import com.uni.lab.domain.Users;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public class UserServiceImpl implements UserService{
    
    @Autowired
    private GenericDao<Users> genericDao;
    
    public static final String CURRENT_USER = "currentUser";

    public void setGenericDao(GenericDao<Users> daoToSet) {
        genericDao = daoToSet;
        genericDao.setClazz(Users.class);
    }
    
    @Override
    public ModelAndView checkLoginStatusAndReturnModel(String currentModelName, HttpServletRequest request) {
        ModelAndView model = new ModelAndView(currentModelName);
        if (checkLoginStatus(request)) {
            return new ModelAndView("login");
        }

        model.addObject("user", request.getSession().getAttribute(CURRENT_USER));
        return model;
    }
    
    @Override
    public Boolean checkLoginStatus(HttpServletRequest request) {
        return (request.getSession().getAttribute(CURRENT_USER) == null);
    }
    
    @Override
    public void saveUser(Users user) {
        genericDao.update(user);
    }
}
