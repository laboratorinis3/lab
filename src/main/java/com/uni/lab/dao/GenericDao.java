package com.uni.lab.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

public interface GenericDao<T extends Serializable> {

    void setClazz(Class<T> clazz);

    Session getCurrentSession();

    Integer create(T o);

    T read(Integer id);

    void update(T o);

    void delete(T o);

    List<T> getAll(final Class<T> o);

}
