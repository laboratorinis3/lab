package com.uni.lab.dao;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("GenericDao")
@Transactional
public class GenericDaoImpl<T extends Serializable> implements GenericDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> clazz;

    public void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Integer create(T o) {
        return (Integer) getCurrentSession().save(o);
    }

    @Override
    public T read(Integer id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    @Override
    public void update(T o) {
        getCurrentSession().update(o);
    }

    @Override
    public void delete(T o) {
        getCurrentSession().delete(o);
    }

    @Override
    public List<T> getAll(final Class<T> o) {
        List<?> list = getCurrentSession().createCriteria(o).list();
        return (List<T>) (list == null ? Collections.emptyList() : list);
    }

}
