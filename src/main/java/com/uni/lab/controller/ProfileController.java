package com.uni.lab.controller;

import com.uni.lab.domain.Users;
import com.uni.lab.services.UserService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProfileController {

    public static final String CURRENT_USER = "currentUser";
        public static final String PROFILE = "profile";


    @Autowired
    private UserService userService;

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView profile(HttpServletRequest request) {
        return userService.checkLoginStatusAndReturnModel(PROFILE, request);

    }

    @RequestMapping(value = "/profile/save", method = RequestMethod.POST)
    public ModelAndView profileSave(HttpServletRequest request,
            @RequestParam("firstname") String firstname,
            @RequestParam("lastname") String lastname,
            @RequestParam("email") String email) {
        Users user = (Users) request.getSession().getAttribute(CURRENT_USER);
        if (!userService.checkLoginStatus(request)) {
            user.setFirstname(firstname);
            user.setLastname(lastname);
            user.setEmail(email);
            userService.saveUser((Users) request.getSession().getAttribute(CURRENT_USER));
        }
        return userService.checkLoginStatusAndReturnModel(PROFILE, request);
    }
}
