package com.uni.lab.controller;

import com.uni.lab.dao.GenericDao;
import com.uni.lab.domain.Users;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @Autowired
    private GenericDao<Users> genericDao;

    public void setGenericDao(GenericDao<Users> daoToSet) {
        genericDao = daoToSet;
        genericDao.setClazz(Users.class);
    }

    @RequestMapping(value = "/checklogin", method = RequestMethod.POST)
    public ModelAndView checklogin(HttpServletRequest request,
            @RequestParam("email") String email,
            @RequestParam("password") String password) {

        for (Users user : genericDao.getAll(Users.class)) {
            if (email.equals(user.getEmail()) && password.equals(user.getPassword())) {
                request.getSession().setAttribute("currentUser", user);
                return new ModelAndView("redirect:/");
            } else if (email.equals(user.getEmail())) {
                return new ModelAndView("login");
            }
        }

        Users user = new Users(email, password);
        genericDao.create(user);

        request.getSession().setAttribute("currentUser", user);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest request) {
        request.getSession().setAttribute("currentUser",  null);
        return new ModelAndView("redirect:/");
    }
}
