package com.uni.lab.controller;

import com.uni.lab.dao.GenericDao;
import com.uni.lab.domain.Users;
import com.uni.lab.services.UserService;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private GenericDao<Users> genericDao;

    public void setGenericDao(GenericDao<Users> daoToSet) {
        genericDao = daoToSet;
        genericDao.setClazz(Users.class);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request) {
        return userService.checkLoginStatusAndReturnModel("home", request);
    }
}
